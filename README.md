# FindWithSkips

```
Write a function that computes the number of matches between two strings.

   h = “this is a sample haystack”
   n = “isa”

A match is when the string n can be found inside h with skips:

   h = “this is a sample haystack”
          ^^    ^                     (1)
          ^       ^^                  (2)
             ^              ^ ^       (3)
                 ...
```
