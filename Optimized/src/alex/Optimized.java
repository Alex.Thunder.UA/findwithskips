package alex;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Optimized {

    private static long FindMatches(String sSource, String sMatch) {
        // Trim redundant leading and trailing characters
        int chFirst = sSource.indexOf(sMatch.charAt(0));
        int chLast  = sSource.lastIndexOf(sMatch.charAt(sMatch.length() - 1));
        if (chFirst < 0 || chLast < 0 || chLast - chFirst + 1 < sMatch.length())
            return 0;

        sSource = sSource.substring(chFirst, chLast + 1);
        System.out.println("Reduced source string: " + sSource);

        int  iLastChar = sSource.length() - 1; // That will be moving to the beginning depending on findings below
        long iCurCount = 0;
        char cMatchChar;
        boolean bLastChar;

        // Number of matchings since Source position for previously explored character
        TreeMap<Integer, Long> prevTreePosCount = null;
        for (int i = sMatch.length() - 1; i >= 0; --i) {
            cMatchChar = sMatch.charAt(i);
            iCurCount = 0;
            bLastChar = true;

            // Number of matchings since Source position for currently explored character
            TreeMap<Integer, Long> curTreePosCount = new TreeMap<>();

            for (int j = iLastChar; j >= 0; --j) {
                if (cMatchChar != sSource.charAt(j))
                    continue;

                if (prevTreePosCount == null) // No prevTreePosCount yet
                    ++iCurCount;
                else {
                    // Find number of continuations since the current position
                    Map.Entry<Integer, Long> e = prevTreePosCount.higherEntry(j);
                    if (e == null) // No matching continuations, this position won't match
                        continue;

                    iCurCount += e.getValue();
                }

                curTreePosCount.put(j, iCurCount);

                if (bLastChar) {        // No sense to explore starting with this position on next iteration
                    iLastChar = j - 1;  // Shift the last explored char left for the next match search
                    bLastChar = false;
                }
            }

            if (iCurCount == 0) // No at all suitable matches for current character
                break;

            prevTreePosCount = curTreePosCount;
        }

        return iCurCount;
    }

    public static void main(String[] args) {
        // Initialize strings
        String sSource;
        String sMatch;

        // Prompt for strings if not passed
        if (args.length < 2) {
            Scanner scan = new Scanner(System.in);

            System.out.print("Enter a source string: ");
            sSource = scan.nextLine();

            System.out.print("Enter a match string: ");
            sMatch = scan.nextLine();
        }
        else {
            sSource = args[0];
            sMatch  = args[1];
        }

        if (sSource.isEmpty() || sMatch.isEmpty()) {
            System.out.println("Both strings should not be empty.");
            return;
        }

        System.out.println(sSource + " <- " + sMatch);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Instant instStart = Instant.now();
        System.out.println("Number of matches: " + String.valueOf(FindMatches(sSource, sMatch)));
        System.out.println("Elapsed time: " + Duration.between(instStart, Instant.now()));
    }
}
