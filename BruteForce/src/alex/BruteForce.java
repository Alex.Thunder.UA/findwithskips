package alex;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class BruteForce {

    private static Boolean bTraceFindings = false; // Enable for debug purpose visualization

    private static int FindMatches(String sSource, int iSource, String sMatch, int iMatch) {

        if (iMatch >= sMatch.length())  // Matched the whole string
            return 1;

        if (iSource >= sSource.length())// Reached the end of source string and did not find a match
            return 0;

        int nRet = 0;

        // Find the next match symbol (cLookFor) in sSource starting with iSource position and proceed to next in recursion
        char cLookFor = sMatch.charAt(iMatch);
        while(iSource < sSource.length())
        {
            if (sSource.charAt(iSource) == cLookFor) {
                if (bTraceFindings) {
                    for (int i = 0; i < iSource; i++)
                        System.out.print(" ");
                    System.out.println(cLookFor);
                }

                int nAdd = FindMatches(sSource, iSource + 1, sMatch, iMatch + 1);
                if (nAdd > 0) {
                    if (bTraceFindings && iMatch == sMatch.length() - 1)
                        System.out.println(" + ");

                    nRet += nAdd;
                }
            }

            ++iSource;
        }

        return nRet;
    }

    public static void main(String[] args) {
        // Initialize strings
        String sSource;
        String sMatch;

        // Prompt for strings if not passed
        if (args.length < 2) {
            Scanner scan = new Scanner(System.in);

            System.out.print("Enter a source string: ");
            sSource = scan.nextLine();

            System.out.print("Enter a match string: ");
            sMatch = scan.nextLine();
        }
        else {
            sSource = args[0];
            sMatch  = args[1];
        }

        System.out.println(sSource + " <- " + sMatch);
        System.out.println("=========================");

        Instant instStart = Instant.now();
        System.out.println(String.valueOf(FindMatches(sSource, 0, sMatch, 0)));
        System.out.println("Elapsed time: " + Duration.between(instStart, Instant.now()));
    }
}
